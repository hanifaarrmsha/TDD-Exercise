from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Hanifa Arrumaisha' # TODO Implement this
yearNow = date.today().year

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(1998)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    return yearNow-birth_year;
